#!/usr/bin/python

# need root privileges

import struct
import sys
import time
import re

from socket import AF_INET, AF_INET6, inet_ntoa

sys.path.append('python')
sys.path.append('build/python')
import nfqueue

sys.path.append('dpkt-1.6')
from dpkt import ip, udp, hexdump

q = nfqueue.queue()
q.open()
print "Closing Queue"
res = q.unbind(AF_INET)
print "Result: %s" % res
exit(res)
