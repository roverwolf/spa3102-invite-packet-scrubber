#!/usr/bin/python

# need root privileges

import struct
import sys
import time
import re

from socket import AF_INET, AF_INET6, inet_ntoa

sys.path.append('python')
sys.path.append('build/python')
import nfqueue

sys.path.append('dpkt-1.6')
from dpkt import ip, udp, hexdump
debug = 2

def cb(i,payload):
  if debug: print "\n\nNew Packet Received. Size: : %s"  % payload.get_length()
  data = payload.get_data()
  pkt = ip.IP(data)
  if debug: print "proto:", pkt.p
  if debug: print "source: %s" % inet_ntoa(pkt.src)
  if debug: print "dest: %s" % inet_ntoa(pkt.dst)
  if pkt.p == 17:
    if debug: print "  sport: %s" % pkt.udp.sport
    if debug: print "  dport: %s" % pkt.udp.dport
    sdata = str(pkt.udp.data)
    if debug == 2: print "data: ", sdata
    if sdata.find("INVITE",0,10) != -1 or sdata.find("BYE",0,10) != -1 or sdata.find("ACK",0,10) != -1:
      print "Packet from %s may need modification" % inet_ntoa(pkt.src)
      pkt2 = pkt
      old_len = len(pkt2.udp.data)
      if debug: print "Len: %s, PLen: %s" % (old_len, pkt2.len)
      #pkt2.udp.data = "GET /\r\n"
      matcher = re.compile('((?:From|Remote-Party-ID):\s*?)\s([^"]*?)\s<')
      if debug: print "Pre-match"
      match_obj = matcher.search(str(pkt2.udp.data))
      if match_obj:
        if debug: print "Matched"
        # Substitute the spaces for quotes now
        pkt2.udp.data = matcher.sub(r'\1"\2"<', str(pkt2.udp.data))
        if debug == 2: print "New Data: %s" % pkt2.udp.data

        pkt2.len = pkt2.len - old_len + len(pkt2.udp.data)
        if debug: print "pkt.len: %s" % pkt.len
        if debug: print "pkt2.len: %s" % pkt2.len
        if debug: print "len(pkt2): %s" % len(pkt2)

        # I don't know what setting the sum to zero does, it was in the
        # Examples, though
        pkt2.udp.sum = 0
        pkt2.sum = 0

        ret = payload.set_verdict_modified(nfqueue.NF_ACCEPT, str(pkt2), len(pkt2))
        if debug: print "ret = ",ret
        return 1
      else:
        if debug: print "Did not match"

  # Accept packets that make it to here
  payload.set_verdict(nfqueue.NF_ACCEPT)
  sys.stdout.flush()
  return 1

q = nfqueue.queue()

if debug: print "open"
q.open()

if debug: print "bind"
q.bind(AF_INET);

#print "setting callback (should fail, wrong arg type)"
#try:
#	q.set_callback("blah")
#except TypeError, e:
#	print "type failure (expected), continuing"

if debug: print "setting callback"
q.set_callback(cb)

if debug: print "creating queue"
q.create_queue(0)

print "trying to run"
try:
	q.try_run()
except KeyboardInterrupt, e:
	print "interrupted"


if debug: print "unbind"
q.unbind(AF_INET)

if debug: print "close"
q.close()

